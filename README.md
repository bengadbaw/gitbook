# Benjamin + Gitlab

---

## About Me

I have about 10 years experience as a practioner and about 3 years as a research manager. 

I found product research while I was managing partnerships for a think tank in Seattle, called the National Bureau of Asian Research. One of our partners, PATH, does work with the development of vaccines and devices, and policy & advocacy for low resource-settings. I was visiting their  product development shop and I realized I wanted to be more hands on with the design and research itself rather than in health policy. 

One of my first hernest experiences in collaborating with an engineer came early. I was designing a dashboard for a team at Microsoft. It included a map with issues across the United States. I had been designing not in isolation, but with little interaction with the underlying data. I talked to one of engineers who explained the general data model and how he was planning on integrating the design work. I realized that he really just needed variants on a color and I was designing the whole dashboard. I sent his the HEX values and we were instantly starting to iterate in real time between the live site and what were photoshop files at the time. 

Eventually I built up enough of a portolio to apply to graduate school and was accepted into the class of 2012 at SVA's Interaction Design program. There I had the opportunity to work with some design and research luminaries including Feltron who designed Facebook's first timeline, Alex Wright, who is now UX Director at Instagram, Zach Klein who founded Vimeo. One of the highlights of my time there was pitching Chris Dixon who is now a General Partner at Andreessen Horowitz on Vicejar, which was a swear jar for bad habits. I built a working prototype and was running an experiment with my classmates. 

After graduate school, at IDEO I had the opportunity to lead research for a multi-year engagement to build out two vehicle simulators for prototyping the HMI for future fleets. 

Built two labs for testing in-vehicle HMIs and then IDEOs first conversational interface for prototyping mobile interfaces. 


